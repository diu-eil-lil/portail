"""
Wa-Tor

author : Formateurs DIU
Département Informatique Université de Lille
"""


import wator
import time 
import pylab  # python -m pip install -U matplotlib

PAUSE = 0.1

def evolve(sea, nb_steps):
    '''
    make wator simulation evolves for nb_steps
    each round a square is randomly picked for evolution
    sea is printed every widthxheight steps
    @param {dict[][]} sea : the sea
    @param {int} nb_steps : nb evolution steps of simulation
    @return list of tuples of the form (step, nb_tunas, nb_sharks)
    '''

    populations = []
    for step in range(nb_steps):
        wator.evolve_one_square(sea)
        populations.append( (step, wator.NB_TUNAS, wator.NB_SHARKS) )
        if (step % (wator.SEA_WIDTH*wator.SEA_HEIGHT) ) == 0:
        #if (step % 100 ) == 0:
            print('step {} | ( {} , {} ) '.format(step, wator.NB_TUNAS, wator.NB_SHARKS))
            print(wator.to_string(sea))
            time.sleep(PAUSE)
    return populations
            
def run_wator(nb_steps, width=30, height=30, tuna_percent = 30, shark_percent = 10):
    '''
    run wator simulation for nb_steps steps
    sea is widthxheight and at beginning there is ~tuna_percent% tuna and ~shark_percent% shark
    @param {int} nb_steps nb evolution steps of simulation
    @param {int} width width of sea (default is 30)
    @param {int} height width of sea (default is 30)
    @param {int} tuna_percent tuna initial percentage
    @param {int} shark_percent shark initial percentage
    @return list of tuples of the form (step, nb_tunas, nb_sharks) for each step of simulation    
    '''
    sea = wator.create_sea(width,height)
    wator.init_sea(sea, tuna_percent, shark_percent)
    populations = evolve(sea, nb_steps)    
    show_curves(populations)
    return populations
    

def show_curves(populations):
    '''
    display the cruves from data in populations
    @param {(idx,x,y)} populations : the data as tuples (step number, x coord, y coord)
    '''
    def build_data_from_populations(populations):
        '''
        @return a tuple of 3 lists ; list of step numbers, list of x coord and list of y coord
        '''
        data_x = []
        data_tuna = []
        data_shark = []
        for d in populations:
            data_x.append(d[0])
            data_tuna.append(d[1])
            data_shark.append(d[2])
        return (data_x, data_tuna, data_shark)
    
    (data_x, data_tuna, data_shark) = build_data_from_populations(populations)
    pylab.plot(data_x, data_tuna)
    pylab.plot(data_x, data_shark)
    pylab.title('évolution des populations de thons et requins')
    pylab.show()
    

# command line the number of simulation steps can be gven as argument, else by defult it is widthxheightx200    
if __name__ == '__main__':
    import sys
    # default values
    width = 25
    height = 25
    nb_steps = width*height*200
    if len(sys.argv) > 1:
        nb_steps = int(sys.argv[1])
        if len(sys.argv) > 2:
            width = int(sys.argv[2])
            height = int(sys.argv[3])
    run_wator(nb_steps, width, height)
    
#run_wator(30*30*200,30,30)   