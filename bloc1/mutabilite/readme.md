---
title: Mutabilité des données
date: juin 2019
author: Équipe pédagogique DIU EIL Lille
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---

**Objectifs :**

* avoir un aperçu de ce que sont les variables en Python
* comprendre le partage de valeurs (aliasing)
* comprendre les effets de ce partage pour des valeurs mutables


**Moyen :**


* [Python tutor](http://pythontutor.com)

# Types de données en Python

En Python, la majorité des objets des types de base ne sont pas mutables :

* nombres (`int`, `float`, `complex`)
* booléens (`bool`)
* chaînes de caractères (`str`)
* $p$-uplets (`tuple`)


mais ceux de  quelques types le sont

* listes (`list`)
* dictionnaires (`dict`)
* ensembles (`set`)


# Variables en Python

* [Première démo avec Python Tutor](http://pythontutor.com/live.html#code=a%20%3D%201%0Ab%20%3D%20a%0Aprint%28'adresses%20des%20valeurs%20de%20a%20et%20b%20%3A%20',%20id%28a%29,%20id%28b%29%29%0Aa%20%3D%20True%0Aprint%28'adresses%20des%20valeurs%20de%20a%20et%20b%20%3A%20',%20id%28a%29,%20id%28b%29%29%0Al1%20%3D%20%5B1,%202,%203%5D%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l1%20%3A%20',id%28l1%29%29%0Al2%20%3D%20l1%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l2%20%3A%20',%20id%28l2%29%29%0Al3%20%3D%20%5B1,%202,%203%5D%0Aprint%28'adresse%20de%20la%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l3%20%3A%20',%20id%28l3%29%29%0At1%20%3D%20%281,%202,%203%29%0Aprint%28'adresse%20du%20tuple%20r%C3%A9f%C3%A9renc%C3%A9%20par%20t1%20%3A%20',%20id%28t1%29%29%0A&cumulative=false&curInstr=13&heapPrimitives=true&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
* Gestion mémoire des valeurs
* les variables sont des références (pointeurs) vers les variables
* certaines valeurs sont référencées par plusieurs variables
* la fonction prédéfinie `id` renvoie l'adresse en mémoire d'une valeur (attention pas de la variable)



# Partage de valeurs et mutabilité

* [Deuxième démo avec Python Tutor](http://pythontutor.com/live.html#code=l1%20%3D%20%5B1,%202,%203%5D%0Al2%20%3D%20l1%0Al3%20%3D%20l1.copy%28%29%0Al2%5B1%5D%20%3D%205%0Aprint%28'Apr%C3%A8s%20modification%20du%20deuxi%C3%A8me%20%C3%A9l%C3%A9ment%20de%20l2'%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l1%20%3A%20',%20l1%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l2%20%3A%20',%20l2%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l3%20%3A%20',%20l3%29%0Al1.append%284%29%0Aprint%28%22Apr%C3%A8s%20ajout%20d'un%20%C3%A9l%C3%A9ment%20en%20fin%20de%20l1%22%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l1%20%3A%20',%20l1%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l2%20%3A%20',%20l2%29%0Aprint%28'*%20liste%20r%C3%A9f%C3%A9renc%C3%A9e%20par%20l3%20%3A%20',%20l3%29%0A&cumulative=false&curInstr=13&heapPrimitives=true&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
* lorsque plusieurs variables référencent une même valeur mutable, toute mutation de cette valeur est visible depuis toutes ces variables
* la méthode `copy` des listes, des dictionnaires, des ensembles permet d'éviter ce problème

# Copie simple ou profonde ?

* [Troisième démo avec Python Tutor](http://pythontutor.com/live.html#code=m1%20%3D%20%5B%5B0%20for%20_%20in%20range%285%29%5D%20for%20_%20in%20range%283%29%5D%0Am2%20%3D%20m1.copy%28%29%0Aprint%28'Au%20d%C3%A9but%20'%29%0Aprint%28'*%20matrice%20m1%20%3A',%20m1%29%0Aprint%28'*%20matrice%20m2%20%3A',%20m2%29%0Am2%5B0%5D%5B0%5D%20%3D%201%0Aprint%28'Apr%C3%A8s%20modification'%29%0Aprint%28'*%20matrice%20m1%20%3A',%20m1%29%0Aprint%28'*%20matrice%20m2%20%3A',%20m2%29%0Afrom%20copy%20import%20deepcopy%0Am3%20%3D%20deepcopy%28m1%29%0Aprint%28'*%20matrice%20m3%20%3A',%20m3%29%0Am3%5B0%5D%5B0%5D%20%3D%201%0Aprint%28'Apr%C3%A8s%20modif%20de%20m3'%29%0Aprint%28'*%20matrice%20m1%20%3A',%20m1%29%0Aprint%28'*%20matrice%20m3%20%3A',%20m3%29%0A&cumulative=false&curInstr=46&heapPrimitives=true&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
* partage de valeurs dans les listes de listes
* la méthode `copy` fait une copie au «1er niveau»
* pour copier «profondément» des listes de listes : fonction `deepcopy` du module `copy`

# Danger d'effets de bord non désirés

* [Quatrième démo avec Python Tutor](http://www.pythontutor.com/live.html#code=def%20fact%28n%29%3A%0A%20%20%20%20res%20%3D%201%0A%20%20%20%20while%20n%20%3E%200%3A%0A%20%20%20%20%20%20%20%20res%20*%3D%20n%0A%20%20%20%20%20%20%20%20n%20-%3D%201%0A%20%20%20%20return%20res%0A%0An%20%3D%204%0Aprint%28fact%28n%29%29%0A%0Adef%20long%28l%29%3A%0A%20%20%20%20res%20%3D%200%0A%20%20%20%20while%20l%20!%3D%20%5B%5D%3A%0A%20%20%20%20%20%20%20%20res%20%2B%3D%201%0A%20%20%20%20%20%20%20%20l.pop%28%29%0A%20%20%20%20return%20res%0A%20%20%20%20%0Al%20%3D%20%5B1,%202,%203%5D%0Aprint%28long%28l%29%29%0A&cumulative=false&curInstr=0&heapPrimitives=nevernest&mode=display&origin=opt-live.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false)
* fonction modifiant la valeur des paramètres 
 
