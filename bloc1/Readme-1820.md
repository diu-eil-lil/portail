Bloc 1 - Représentation des données et programmation
====================================================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)  
[Promotion 2018-20](../Readme-1820.md)

Intervenants - contacts
=======================

Patricia Everaere,
Mikaël Salson,
Benoit Papegay,
Éric Wegrzynowski,
Philippe Marquet,
Jean-Christophe Routier,
Bruno Bogaert,
Maude Pupin,
Jean-François Roos,
Fabien Delecroix,
Jean-Stéphane Varré

9 mai - Premiers pas avec Thonny et Python
==================================

* [Prise en main de Thonny](thonny/readme.md)
* [Traitement de séquences ADN](adn/readme.md)
* [Jeu de la vie](jeu-de-la-vie/readme.md)

15 mai
======

* [Bonnes pratiques de programmation Python](bonnes_pratiques/readme.md)
  - amphi 
  - doctest, mode pas à pas
  - modules 
* [Jeu de la vie](jeu-de-la-vie/readme.md) à terminer
* [Générateur de phrases de passe](phrases_de_passe/readme.md)

Travail pour le 5 juin
======================

* [Wator - simulation proie-prédateur](wator/readme.md)
  - progression avec les élèves

5 juin
======

* [Module](modules/readme.md)
  - amphi
* [Mutabilité des données](mutabilite/readme.md)
  - amphi 
* Évaluation / discussion Wator
  - [Une correction possible](wator/src) qui utilise les dictionnaires pour modéliser les poissons. \
    Usage via la fonction `run_wator` de `watorMain.py`, déclenchée à l'exécution du script avec les valeurs `run_wator(25*25*200,25,25)`.
   ```python
   def run_wator(nb_steps, width=30, height=30, tuna_percent = 30, shark_percent = 10):
    '''
    run wator simulation for nb_steps steps
    sea is widthxheight and at beginning there is ~tuna_percent% tuna and ~shark_percent% shark
    @param {int} nb_steps nb evolution steps of simulation
    @param {int} width width of sea (default is 30)
    @param {int} height width of sea (default is 30)
    @param {int} tuna_percent tuna initial percentage
    @param {int} shark_percent shark initial percentage
    @return list of tuples of the form (step, nb_tunas, nb_sharks) for each step of simulation    
    '''
   ```

17 juin
=======

* [Travail préparatoire](prepa17juin/readme.md) au TP de la semaine du
  17 juin
  - lecture, un (tout) petit exo de programmation
* Cours :
  - les [dictionnaires](cours17juin/dictionnaire.py) (rapidement)
  - quelques [éléments de bonnes pratiques](cours17juin/exemples_codes_wator.md), suite à la programmation de Wator

* [Course des chicons](course_chicon/readme.md)
  - travaux pratiques

18 juin
=======

* Récursivité
  - [notes de cours](recursivite/readme.md)
  - exercices de TD/TP
	[version PDF](recursivite/exo-recursivite.pdf?inline=false)
	/ [version en ligne](recursivite/exo-recursivite.md)
	une [correction possible pour ces exercices](recursivite/src/solution-exo-recursivite.py)
  - [la pile débranchée](pile-debranchee/Readme.md)


19 juin
=======

* Travaux pratiques
  - [Course des chicons](course_chicon/readme.md)
  - Récursivité
    [version en ligne](recursivite/exo-recursivite.md)
    / [version PDF](recursivite/exo-recursivite.pdf?inline=false)

20 juin
=======

* [Représentation des données](repres_donnees-1820/readme.md)
  - cours
* [Codage, décodage base64](base64/readme.md)
  - travaux pratiques

1er juillet
===========

* [Algorithme du min-max](minmax-1820/readme.md#documents)
  - cours
* [Jeux à deux joueurs, algorithme min-max](minmax-1820/readme.md)
  - projet : lecture du sujet, interface

2 juillet
=========

* projet, suite

4 juillet
=========

* projet, suite et fin

* [Introduction aux carnets Jupyter](jupyter/presentation_carnet_jupyter.ipynb)


Divers
======

 * Diversité et unité des langages de programmation : [comparaison d'un même code](comparaison/readme.md) dans différents langages de programmation

Ressources Python
=================

* [Memento Python3](https://perso.limsi.fr/pointal/_media/python:cours:mementopython3.pdf)
* [Python3 en résumé](doc/python3_resume.md)
* [le livre de Gérard Swinnen](http://inforef.be/swi/download/apprendre_python3_5.pdf)
* les cours du FIL, département Informatique de l'Université de Lille, basés sur Python
  - [Informatique au S1](http://portail.fil.univ-lille1.fr/ls1/info)
  - [AP1 au S2](http://portail.fil.univ-lille1.fr/ls2/ap1)
  - [AP2 au S3](http://portail.fil.univ-lille1.fr/ls3/ap2)


Ressources technologies du web
==============================

Nous n'aurons probablement pas le temps d'aborder la partie du programme *Interactions entre l'homme et la machine sur le Web*. Celle-ci avait déjà été, pour la partie côté client du moins, abordée lors de la formation ISN suivie par une majorité d'entre vous.

Pour les autres, nous vous proposons les ressources suivantes pour travailler ces points, et en particulier les bases des langages HTML, CSS et javascript (programmation événementielle notamment).

* [Cours technologies du web1](https://techweb.univ-lille.fr/tw1/index.html) (vidéos et exercices)
* [Activcités proposées lors de la formation ISN](http://isn.fil.univ-lille1.fr/) -
  HTML, CSS, Javascript, etc. 

Canal de discussion matermost dédié à ces aspects _web_

* [mattermost-fil.univ-lille.fr/diu-eil/channels/b1-web](https://mattermost-fil.univ-lille.fr/diu-eil/channels/b1-web)
