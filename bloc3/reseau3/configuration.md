# Installation et configuration d'un réseau informatique, manipulation des protocoles

## 1.- Objectifs

Ces travaux pratiques consistent, dans un premier temps, à installer et configurer des ordinateurs au sein d'un réseau local. Nous utiliserons ici plus particulièrement des micro-ordinateurs Raspberry Pi fonctionnant avec le système d'exploitation Linux. Une fois les réseaux locaux fonctionnels, nous les interconnecterons par l'intermédiaires de routeurs. Nous manipulerons ensuite différents protocoles réseaux (HTTP, Telnet, ...) et nous aborderons quelques aspects liés à la sécurité informatique.

## 2.- Configuration des réseaux locaux

La figure ci-dessous représente l'architecture réseau complète.
![](figs/archi.png)

Vous disposez chacun d'un micro-ordinateur Raspberry Pi fonctionnant avec le système d'exploitation Linux. Par groupe de quatre, vous allez installer et configurer un réseau local. Pour cela, vous disposez d'un commutateur et d'un routeur. Les routeurs ont préalablement été configurés. L'interface Ethernet 0/0 du commutateur correspond à la connexion vers vers réseau local. L'interface Ethernet 0/1 correspond à l'interconnexion des routeurs entre-eux par l'intermédiaire d'un hub.
![](figs/interface_routeur.png)

Chaque groupe dispose d'une classe C pour son réseau. Seule l'adresse du routeur est fixée, vous pouvez utiliser l'adresse de votre choix pour votre Raspberry Pi. Le tableau suivant indique les différents réseaux à utiliser.

| Numéro de groupe | Adresse de réseau | Masque de sous-réseau | Adresse du routeur |
| :--------------: | :---------------: | :-------------------: | :----------------: |
| 1 | 192.168.1.0 | 255.255.255.0 | 192.168.1.1 |
| 2 | 192.168.2.0 | 255.255.255.0 | 192.168.2.1 |
| 3 | 192.168.3.0 | 255.255.255.0 | 192.168.3.1 |
| 4 | 192.168.4.0 | 255.255.255.0 | 192.168.4.1 |


### Configuration des Raspberry Pi

Pour réaliser la configuration, vous utiliserez la commande shell *ip*. Il faut pour cela être administrateur de la machine (utilisateur *root*). Pour cela, utilisez la commande :

    sudo su -


Pour débuter, regardons la configuration actuelle. La commande `ip address` (qui peut être réduite à `ip a`) permet de visualiser la configuration actuelle de toutes les interfaces réseaux de la machine. La première interface, *lo*, correspond à l'interface de *loopback*. La deuxième interface, *eth0*, correspond à la carte Ethernet. Au démarrage, cette carte n'est pas configurée et n'est pas démarrée. Pour ajouter une adresse IP à l'interface *eth0*, on utilise la commande

    ip address add X.Y.Z.W/MM dev eth0

Puis pour démarrer l'interface réseau :

    ip link set dev eth0 up

Pour vérifier la configuration, on utilisera à nouveau la commande 

    ip a

Une fois que vous avez réalisé la configuration de votre Raspberry Pi et que vous l'avez connecté au commutateur, vous pouvez tester le bon fonctionnement en utilisant la commande `ping` à destination du routeur puis de d'une autre Raspberry de votre réseau. Par exemple :

    ping 192.168.G.1

Si le routeur ne vous répond pas, il faut commencer par vérifier la connectivité physique puis revérifier la configuration de votre adresse.

Afin de pouvoir communiquer avec les machines situées dans un autre réseau, vous devez ajouter un routeur de sortie.
Pour visualisez la table de routage de votre machine, vous pouvez utiliser la commande :

    ip route

Vous ne devriez observer qu'une seule ligne correspondant au réseau local dans lequel vous êtes. Afin d'accéder aux autres réseaux, il faut ajouter une route par défaut passant par votre routeur. La commande sera par exemple :

    ip route add default via 192.168.G.1 dev eth0

Pour tester le bon fonctionnement, vous pouvez de nouveau utiliser la commande `ping` mais cette fois à destination d'un routeur distant  comme par exemple :

    ping 192.168.X.1


### Configuration des routeurs

Les routeurs utilisent le protocole RIP (version 2) pour s'échanger leurs tables de routages. Afin d'observer l'échange des messages entre les routeurs, nous avons placé un Raspberry Pi sur le hub d'interconnexion des routeurs. Celui permet de récupérer toutes les trames qui circulent en utilisant le logiciel *wireshark*.

Dans un premier temps, vous pouvez utiliser la commande  `ping` pour tester la connectivité avec les différents hôtes (routeurs, autres postes d'autres réseaux), et vérifier ainsi le fonctionnement des réseaux interconnectés. Vous pouvez également consulter la valeur du TTL obtenue en retour, à comparer à la valeur utilisée par défaut par ping (64).

Dans un second temps, vous pouvez utiliser la commande  `traceroute` pour connaître les interfaces visibles (lesquelles ?) des routeurs qui vous donnent l'accès à un poste distant. Le mécanisme utilisé par ce dernier est hors programme, mais peut être intéressant à comprendre via le logiciel *wireshark*.


## 3.- Utilisation de services

### Première récupération d'informations

Sur votre Raspberry Pi, développer un formulaire HTML, demandant par exemple un identifiant et un mot de passe, et une page PHP pour traiter les données provenant du formulaire. Lancez *wireshark* et demandez à votre voisin d'accéder à votre formulaire et de le remplir. Quelles sont les informations que vous voyez passer (wireshark) ?

La racine du serveur web se situe dans le répertoire `/var/www/html/`, le formulaire HTML et la page PHP peuvent s'inspirer de

`form.html`

```
<html>
  <body>
    <form action="post.php" method="post">
      Entrez votre login : <input type="text" name="login" /><br />
      Entrez votre mot de passe : <input type="password" name="mdp" /><br />
      <input type="submit" />
    </form>
  </body>
</html>
```

et de

`post.php`

```
<html>
  <body>
    votre nom : <?php echo '\"' . htmlspecialchars($_POST["login"]) . '\" '; ?>   <br/>
    votre mot de passe : <?php echo '\"' . htmlspecialchars($_POST["mdp"]) . '\" ';  ?>  <br/>
    (votre adresse IP :  <?php echo 'User IP Address - '.$_SERVER['REMOTE_ADDR']; ?> ) <br/>
  </body>
</html>
```





### Manipulation des protocoles


#### Connexion à distance en mode texte

Lancez *wireshark* sur votre raspberry, puis connectez-vous à la machine d'une autre personne en utilisant la commande `telnet`. Le mot de passe de l'utilisateur *pi* est *diu*. Que remarquez-vous ?
Réalisez la même opération en utilisant cette fois-ci la commande `ssh`. Que constatez-vous au niveau de la sécurité des différentes commandes vis-à-vis d'un éventuel pirate ?

#### Requête HTTP en mode texte

Lancez *wireshark* sur votre raspberry, puis connectez-vous au serveur web d'une autre personne en utilisant la commande `telnet @IP_distante 80`, où `80` designe le port du serveur web sur la machine distante. 
Sur votre terminal  `telnet`, lancez la requête HTTP suivante, suivie d'un *double entrée*:

```
GET / HTTP/1.0
```
Sur votre terminal `telnet`, quelles informations de l'entête HTTP, puis du contenu HTML sont distinguables pour la requête, ainsi que sa réponse ?
Sur *wireshark* , quelles informations supplémentaires peuvent être obtenues, au niveau des couches supportant la couche applicative (consultez les contenus TCP,IP ainsi qu'Ethernet) ?

<!-- eof -->
