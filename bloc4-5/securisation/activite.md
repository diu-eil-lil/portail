Cette activité à pour but d'aborder de sensibiliser les élèves aux
problèmes de chiffrements.


# Matériels


## Physique

-   Pour le chiffrement symétrique :
    -   le [carré de Vigenère](./figs/tikz_carre_vigenere.pdf), un par élève ;
    -   des [bandes de papier](./figs/bandes.pdf) quadrillé, quelques bandes par élèves;
-   Pour le chiffrement asymétrique ;
    -   des cadenas à code de couleur différentes :
        
        ![img](./figs/cadenas.png)
        
        -   au moins 5 cadenas de chaque couleur.
    -   des boites opaques (autant que de cadenas) que l'on peut fermer
        à l'aide des cadenas, ou des pochettes plastiques (opaques)
        avec un trou pour passer le cadenas.


## Logiciel

-   La [classe](./src/alphabet.py) `Alphabet`
-   Chiffrement de [Vigenere](./src/vigenere.py)
-   Chiffrement [RSA](./src/rsa.py)


# Déroulements de l'activité débranchée


## Chiffrement symétrique de Vigenère

**à Distribuer** :

-   les carrés de Vigenere ;
-   les bandes de papier quadrillées.


### Chiffrement

Pour chiffrer un message , on choisi une **clé** sous la forme d'un
mot constitué des 26 lettres de l'alphabet latin.

Par exemple `DIUEIL`

On dispose ensuite :

-   le message à chiffrer (le **clair**) sur la première ligne  ;
-   la clé, éventuellement répétée, sur la deuxième ligne

Le **chiffré** prendra place sur la troisième ligne.

Pour chaque lettre \(c\) du clair, et chaque lettre \(l\) de la clé,
on repère dans la table de Vigenère la cellule située aux
coordonnées \((c, l)\). La lettre du chiffré est le contenu de cette
cellule.

Par exemple :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">S</th>
<th scope="col" class="org-left">O</th>
<th scope="col" class="org-left">L</th>
<th scope="col" class="org-left">U</th>
<th scope="col" class="org-left">T</th>
<th scope="col" class="org-left">I</th>
<th scope="col" class="org-left">O</th>
<th scope="col" class="org-left">N</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">D</td>
<td class="org-left">I</td>
<td class="org-left">U</td>
<td class="org-left">E</td>
<td class="org-left">I</td>
<td class="org-left">L</td>
<td class="org-left">D</td>
<td class="org-left">I</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left">V</td>
<td class="org-left">W</td>
<td class="org-left">F</td>
<td class="org-left">Y</td>
<td class="org-left">B</td>
<td class="org-left">T</td>
<td class="org-left">R</td>
<td class="org-left">V</td>
</tr>
</tbody>
</table>

**analyse** : 

-   les deux `O` sont chiffrés par des lettres différentes ;
-   le `S` et le `V` sont tout deux chiffrés en `V` ;
-   Il s'agit d'une **substitution polyalphabétique** ;
-   Le décryptage par analyse de fréquence devient difficile.


### Déchiffrement

**Proposer** aux élèves de déchiffrer un message.

**Méthode** : La méthode de déchiffrement est plus délicate. Pour
 chaque lettre \(k\) du chiffré et chaque lettre \(l\) de la clé qui
 lui correspond :

-   je cherche dans la ligne \(l\), la lettre \(k\). Elle se situe à
    la colonne \(c\) ;
-   la lettre située dans la première ligne de la colonne \(c\) est
    la lettre du clair correspondante.

**Exemple** : déchiffrer le message suivant :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">F</th>
<th scope="col" class="org-left">W</th>
<th scope="col" class="org-left">H</th>
<th scope="col" class="org-left">J</th>
<th scope="col" class="org-left">Q</th>
<th scope="col" class="org-left">Y</th>
<th scope="col" class="org-left">H</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">D</td>
<td class="org-left">I</td>
<td class="org-left">U</td>
<td class="org-left">E</td>
<td class="org-left">I</td>
<td class="org-left">L</td>
<td class="org-left">D</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>


### Communiquer la clé

**Problématique** Alice (A) et Bob (B) (ou Ahmed et Brittany si vous
 voulez) souhaitent échanger un message de manière sécurisée à
 l'aide du chiffrement de Vigenère. Ils n'ont pour l'instant
 convenu d'aucune clé.

**Interrogation** 

-   Comment échanger la clé ?
-   Peut-on utiliser le même canal pour communiquer les messages et
    la clé ?
-   Le canal choisi est-il sûr ?


### Trop de clé

**Problématique** Si tous les élèves de la classe souhaitent
 communiquer de manière sécurisée, combien de clés sont nécessaires ?


## Chiffrement asymétrique

On dispose de \(n\) couleurs de cadenas, et de \(k\) cadenas par
couleurs.

**à faire** :

-   choisir le groupe \(M\) des élèves qui vont recevoir les messages
    (autant d'élèves que de couleur de cadenas) et confier à chaque
    élève de ce groupe \(n\) cadenas de la même couleur ;
-   les élèves du groupe \(M\) choisissent un code secret pour leurs \(n\)
    cadenas : chaque cadenas est initialisé avec ce code,
-   choisir le groupe \(C\) les élèves qui vont chiffrer 
    (autant d'élèves que de cadenas)
-   choisir le groupe \(T\) des élèves qui vont servir à transporter
    les messages. Ils sont bien entendu mal intentionnés et à
    l'écoute de tous les secrets qui leur permettront de lire les
    message.


### Premier scénario

-   un élève du groupe \(C\) souhaite envoyer un message de manière
    sécurisée à un élève du groupe \(M\). ;
-   Il lui demande un cadenas et prend une boite ;
-   Le destinataire lui confie un cadenas **ouvert** ;
-   Il écrit son message sur une bande de papier, le met dans la
    boite, et ferme la boite avec le cadenas ;
-   Il confie la boite à un élève du groupe \(T\), qui doit
    transporter le message à l'élève destinataire du message.
-   L'élève destinataire du message utilise sa combinaison pour 
    dévérouiller la boite et prendre connaissance du message.

**Analyse** 

-   Il s'agit d'un système de chiffrement asymétrique ;
-   Le cadenas joue le rôle de la clé publique, chacun peut en
    obtenir un (dans la réalité on peut copier une clé publique)
-   Le code secret joue le role de la clé privée, il doit être tenu
    secret.

**Limites**

-   On ne peut pas chiffrer un message avec la clé privée;
-   Dans les systèmes de chiffrement asymétrique, les clés ont le
    même statut : on peut chiffrer avec la clé publique et
    déchiffrer avec la clé privée, &#x2026;


### Deuxième scénario

Dans cette partie, seul un cadenas est autorisé par élève du
groupe \(C\). Par contre, la combinaison du cadenas (modifiée pour
éviter les confusions) est publique.

-   Un élève du groupe \(M\) souhaite communiquer un message à un
    élève du groupe \(C\), par forcément de manière sécurisée.
-   Les élèves du groupe \(T\) peuvent modifier les messages.
-   Les élèves du groupe \(C\) veulent être sûr que les messages
    provient bien de son auteur.

**Problème** 

-   Il faut **signer** le message;
-   Comment construire une signature numérique ?

**Une solution**

-   L'auteur Alice du message calcule un condensé du message de la manière
    suivante :
    -   elle convertit chaque lettre du message en entier entre 0 et
        25 ;
    -   elle calcule la somme \(S\) de tous ces nombres, puis le reste
        de \(S\) par 26. C'est ce nombre qui va servir de condensé.
-   Elle transmet ensuite à Bob ce nombre dans une boite **fermée**
    par le cadenas, ainsi que le message.
-   Bob calcule à son tour un condensé de ce dernier selon le même
    algorithme.
-   Il dévérouille ensuite la boite grace à la combinaison
    (publique) et compare les résultats. Si les résultats concorde,
    alors c'est que le message provient bien de Alice.


### Troisième scénario

Lors du premier scénario, un élève du groupe \(T\), dispose de
cadenas.

-   intercepte le cadenas ouvert envoyé et substitue le sien ;
-   Il reçoit le message dans une boite fermée avec son propre cadenas ;
-   Il dévérouille le cadenas, lit le message et utilise le cadenas
    intercepté pour transmettre un autre message au destinataire
    initial, qui ne se doute de rien.


## Synthèse

-   vocabulaire : clair, chiffré, clé, chiffrement symétrique asymétrique ;
-   Notions de clé publique, privée ;
-   Souligner que, dans la réalité, les deux systèmes sont utilisés **conjointement** :
    -   On utilise un système asymétrique pour transmettre une clé de
        chiffrement symétrique (dite **clé de session**)
    -   On utilise un système de chiffrement symétrique pour le reste
        des échanges.
-   Pourquoi ? en raison du coût des systèmes asymétriques


# Travaux pratiques


## La classe Alphabet

-   L'utilisation des fonctions `ord` et `chr` posent problèmes car
    les alphabets doivent être contigü (ou sinon les alorithmes n'ont
    pas de formulation simples)
-   **première solution** Utiliser une constante `ALPHABET` contenant
    l'alphabet sous la forme d'une chaîne de caractères. On utilise
    alors l'indexation sur les chaînes de caractère et la méthode
    `index`.
-   **deuxième solution** Utiliser une classe dédiée contenant des
    alphabets prédéfinis.


## Vigenere

-   Faire compléter le script <./src/vigenere.py>, fourni ici.
-   Décrypter un chiffré de longueur \(n\) en connaissant la longueur
    \(k\) de la clé avec \(k\) petit devant \(n\).


## RSA

-   fournir les fonctions permettant de 
    -   calculer une clé ;
    -   chiffrer ;
    -   déchiffer.
-   Insister sur : 
    -   le fait que RSA permet de chiffrer **des nombres** ;
    -   pour être sécurisé, la taille de la clé doit être grande (2048 bits au moins) ;
    -   le coût est très élevé.

