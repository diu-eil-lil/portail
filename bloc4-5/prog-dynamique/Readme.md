# Programmation dynamique

* [support de présentation](./ctd/cours_pd_NSI.pdf) (fichier PDF)
* [support d'exercices](./ctd/TD_PD.md) 

Projet Redimensionnement d'images

* [sujet de projet](tp-image/Readme.md)
  * [image surfeur](tp-image/surfer.jpg)
  * [proposition de solution](tp-image/minimal_seams_soluce.py)

_pour mémoire, Projet COVID réalisé avec la 1re promo du DIU_

* [sujet de projet](tp-covid/Projet_COVID_PD.md)
* fichiers Python
    * [edition_squelette.py](tp-covid/edition_squelette.py) 
    * [lecture.py](tp-covid/lecture.py) 
    * [matrice.py](tp-covid/matrice.py)
* [répertoire des fichiers FASTA](./tp-covid/covid/)
