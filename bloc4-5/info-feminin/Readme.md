## Les filles dans les filières du numérique

Présentation par Maude  
inspirée d’une formation PAF, Académie de Lille

Patricia Everaere, Aslı Grimaud, Marie Guichard, Philippe Marquet, Mohamed Nassiri, Maude Pupin

Support de présentation
* [PDF](infofem_DIU-EIL.pdf)
* [source Markdown](infofem_DIU-EIL.md)
* [images](../info-feminin)

