# (cc) Creative Commons bruno.bogaert@univ-lille.fr

import sqlite3
fichierDonnees='injection.db'
conn = sqlite3.connect(database=fichierDonnees)

cur = conn.cursor()
id = input('indiquez un identifiant : ')
while id !="" :
    # utiliser les "placeholders" de sqlite3 pour éviter l'injection SQL
    cur.execute(
      "select nom from users where ident=? ", [id]
    )
    res = cur.fetchone()
    if res is None :
        print(f"aucun utilisateur d'identifiant {id}")
    else :
        print(f"L'utilisateur d'identifant {id} s'appelle {res[0]}")
    id = input('indiquez un identifiant : ')

conn.close()


