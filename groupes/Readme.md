Groupes
=======

[DIU Enseigner l'informatique au lycée](../Readme.md)
* [promotion 2018/19-2020/21](Readme-1820.md)

Promotion 2020/21-2021/22
-------------------------

* 2 groupes : groupe 1 et groupe 3 (oui, 3) 
  [2020-22-diu-groupes.csv](2020-22-diu-groupes.csv)
* fiche émargement
  [OpenDocument](2020-22-diu-emargement.ods)
  / [PDF](2020-22-diu-emargement.pdf)

| nom              | prénom          | groupe |
| ---------------- | --------------- | ------ |
| ACROUTE          | Denis           | 1      |
| BAROIS           | David           | 1      |
| BEAUCOURT        | Frédéric        | 1      |
| BELIN            | Alexis          | 1      |
| BOUACHRA         | Bachir          | 1      |
| BOURRADA         | Brahim          | 1      |
| CARON            | Maxime          | 1      |
| CASSEZ           | Olivier         | 1      |
| COTTEL           | Thierry         | 1      |
| CRITELLI         | Massimo         | 1      |
| D AUBUISSON      | Stéphane        | 1      |
| DELALLEAU        | Éric            | 1      |
| DESESQUELLES     | Francois        | 1      |
| DHAUSSY          | Gilles          | 1      |
| DJAMAI           | Benaouda        | 1      |
| DUCHEMIN         | Frédéric        | 1      |
| DUDA             | Aurélie         | 1      |
| EUDES            | Fabrice         | 1      |
| FALEMPIN         | Émilie Agnès    | 1      |
| FROMENTIN        | Arnaud          | 1      |
| GALLOT           | Pierre-François | 1      |
| GERMAIN          | Philippe        | 1      |
| GHESQUIERE       | Cécile          | 1      |
| GUILLON          | Gaëtan          | 1      |
| LAGODZINSKI      | Rémi            | 3      |
| LE MIEUX         | Vincent         | 3      |
| LEMAIRE          | Mélanie         | 3      |
| LIBBRECHT        | Pierre          | 3      |
| LIBERT           | Frédéric        | 3      |
| MACKE            | Anthony         | 3      |
| MARKEY           | Benoît          | 3      |
| MARTIN           | Stephan         | 3      |
| MASSIET          | Arnaud          | 3      |
| MISLANGHE        | Alexandre       | 3      |
| MISSUWE          | Stéphane        | 3      |
| MORAGUES         | Arnaud          | 3      |
| MOUYS            | Ludovic         | 3      |
| NEUTS            | François        | 3      |
| NOTREDAME        | Nicolas         | 3      |
| PLANQUART        | Patrick         | 3      |
| PRZYSZCZYPKOWSKI | Jean-Marc       | 3      |
| QUENTON          | Denis           | 3      | 
| RAMSTEIN         | Messaline       | 3      |
| RITZ             | Florent         | 3      |
| ROLLOT           | Frédéric        | 3      |
| VANROYEN         | Jean-Philippe   | 3      |
| WIATR            | Véronique       | 3      |
| WISSART          | Rémi            | 3      |

<!-- eof --> 
