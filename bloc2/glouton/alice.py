# -*- coding: utf-8 -*-
"""
:author: DIU-Bloc2 - Univ. Lille
:date: juin 2019

Algorithme de résolution du problème « le repas d'Alice » :
  Alice veut organiser une fête et souhaite inviter le plus de monde
  possible. Elle a dressé la liste de toutes ses amies mais elle
  aimerait que chaque invitée en connaisse au moins 5 autres
  ET qu'au moins 5 autres lui soient inconnues.
  
Formalisation :
Donnée : une liste l d'amis et une relation r de «connaissance» entre ces amis.
Résultat : une liste d'invités, sous-liste de la liste d'amis, dans laquelle chaque invité en connaît au moins 5 autres
           et où au moins 5 autres lui sont inconnus
Objectif : maximiser la longueur de la solution (i.e. le nombre d'invités)
    

Le problème est résolu par un algorithme glouton que l'on peut décrire en pseudo-code par
-------------------------------------------------------------------------------------------------
    s <- l
    chercher dans s un ami qui connaît moins (str.) de 5 membres de s OU  dont moins (str.) de 5 membres lui sont inconnus
    tant qu 'il existe un tel ami :
        s <- s privée de cet ami
        mettre à jour la relation de connaissance entre les éléments de s
        chercher dans s un ami qui connaît moins (str.) de 5 membres de s OU  dont moins (str.) de 5 membres lui sont inconnus
    fin de tant que
    return s
-------------------------------------------------------------------------------------------------

L'algorithme part de la liste de tous les amis et procède par éliminations successives.
On remarque que quand un membre de la liste ne vérifie pas l'une des conditions pour y rester, cette situation est définitive,
(puisque le nombre d'invités ne fait que décroître)
En effet
 - s'il connait moins de 5 autres invités, cela ne peut changer puisqu'on n'ajoute jamais de nouveaux invités
 - idem si moins de 5 invités lui sont inconnus
 
Quand on trouve un ami qui ne vérifie pas les conditions, il doit donc être retiré, ce n'est jamais un mauvais choix.
Le problème admet une solution optimale unique.

Question philosophique : existe-t-il toujours une solution au problème ?
Tout dépend de la façon de le poser. 
1) il se peut qu'Alice ne puisse pas organiser ce repas (par exemple si tous ses amis se connaissent 2 à 2).
En langage usuel on dira qu'il n'y a pas de solution.
2) formalisé logiquement, comme ci-dessus, la liste vide EST une solution.  Ainsi posé, le problème admet toujours une solution.
Mais dans l'exploitation du résultat de l'algorithme on prendra soin de traiter à part le cas où la liste vide est obtenue


Implémentation :
Une présentation assez naturelle des données consiste en une liste d'amis et un graphe de la relation de connaissance
entre eux, graphe donné sous la forme d'une liste de couples

À chaque exécution de la boucle principale deux opérations sont à exécuter :
- la recherche d'un invité qui ne convient pas
- la mise à jour de la relation de connaissance entre eux, après sa suppresion
Si ont les écrivait de façon triviale le coût asymptotique de ces opérations serait proportionnel au carré  du nombre n d'amis
(parcourir l'ensemble des n amis et, pour chacun, les amis  qu'il connait (maxi n-1) , d'où le n*n)
Compte tenu de la boucle principale, le coût global de l'algo serait donc en n*n*n

On peut améliorer l'efficacité en choisissant une représentation de données évitant de recalculer
l'ensemble de la relation après chaque suppression.



Structure de donnée ("connaissances") :
On construit un dictionnaire d'amis. À chaque ami est associé l'ensemble des ses connaissances
parmi les autres amis du dictionnaire.

Le coût d'initialisation  de la structure est en o(n*n) (mais n'est évidemment réalisé qu'une fois)

Quand un invité est supprimé, il faut l'enlever de chaque ensemble d'amis auquel il appartient,
puis supprimer son entrée du dictionnaire.

La suppression d'une entrée de dictionnaire en Python est supposée être en o(1), c'est à dire à temps "constant"
(indépendant de la taille du dico)
Idem pour la suppression d'un élément d'un ensemble (c'est pourquoi on préfère ici des ensembles à des listes)
Le coût de mise à jour du dictionnaire lors d'une suppression est ainsi en o(n)

La recherche dans le dictionnaire d'un invité indésirable est aussi en o(n) (on vérifie chaque ami grâce à un test à coût constant)

La boucle principale et l'algorithme global ont donc un coût asymptotique en o(n*n)

 
"""

def init_connaissances(l,r) :
    """
    : Fabrique un dictionnaire d'amis. À chaque ami est associé l'ensemble de ses connaissances dans le dictionnaire
    :param l: la liste (non vide) des amis
    :param r: relation de connaissance entre membres de l. NB : la relation utilisée sera la complétée de r par symétrie (clôture symétrique)
    :return: dictionnaire associant à chaque membre de l l'ensemble de ses connaissances
    :CU: r est une liste de couples (x,y) telle que x appartient à l, y appartient à l,  x != y.
    
    : coût : o(len(r)) c'est à dire <= o(len(l)^2)
    """
    connaissances = { ami:set() for ami in l}           
    for x,y in r :
        connaissances[x].add(y);
        connaissances[y].add(x);
    return connaissances;

def supprime_connaissance(x, connaissances) :
    """
    :Supprime l'ami x du dictionnaire
    :param x: un ami à supprimer du dictionnaire
    :param connaissances: le dictionnaire
    :CU: x est l'une des clés du dictionnaire 
    Effet de bord : modifie le dictionnaire connaissances
    """
    for ami in connaissances[x] : # coût boucle <= o(nombre de membres du dictionnaire)
        connaissances[ami].remove(x);
    connaissances.pop(x);


def cherche_indesirable(connaissances):
    """
    : Cherche dans le dictionnaire un ami qui ne vérifie pas les conditions d'invitation
    :param liste: liste 
    :param connaissances: dictionnaire associant à chaque membre de la liste l'ensemble de ses connaissances dans cette liste
    :return: un membre de la liste qui connait moins de 5 autres ou ne connait pas moins 5 autres,
    :     ou  None s'il n'y en a pas
    
    coût : o(nombre d'amis dans le dictionnaire)
    """
    plafond = len(connaissances)-5
    for ami in connaissances.keys() :
        if len(connaissances[ami]) < 5 or len(connaissances[ami]) > plafond :
            return ami;
    return None


def status(ami, connaissances) :
    """
        représentation user-friendly  de l'état des relations de connaissance d'une personne
    """
    return f"l'ami {ami} en connaît {len(connaissances[ami])} autre(s) et {len(connaissances)-len(connaissances[ami])} lui sont(est) inconnu(s)"


def invites_possibles(l, r, trace = False) :
    """
    : Résoud le problème des invités d'Alice :
    : construit une sous-liste d'amis invités de façon à ce que chaque invité en connaisse au moins 5 autres
    :      et que au moins 5 autres lui soient inconnus
    : --version purement glouton--
    : et n'en connaisse pas au moins 5 autres
    :param l: liste on vide
    :param r: relation entre membres de l
    :return: liste d'invités
    :CU: r est une liste de couples (x,y) telle que x appartient à l, y appartient à l,  x != y.

    """
    connaissances = init_connaissances(l,r)
    a_ecarter = cherche_indesirable(connaissances)
    while a_ecarter is not None :
        if trace : print(f"{status(a_ecarter,connaissances)} => retiré de la liste");
        supprime_connaissance(a_ecarter,connaissances)
        a_ecarter = cherche_indesirable(connaissances)

    return list(connaissances.keys())


def invites_possibles2(l, r, trace = False) :
    """
    : Résoud le problème des invités d'Alice :
    : construit une sous-liste d'amis invités de façon à ce que chaque invité en connaisse au moins 5 autres
    :      et que au moins 5 autres lui soient inconnus
    : -- version avec test d'arrêt prématuré --
    : et n'en connaisse pas au moins 5 autres
    :param l: liste on vide
    :param r: relation entre membres de l
    :return: liste d'invités
    :CU: r est une liste de couples (x,y) telle que x appartient à l, y appartient à l,  x != y.
    """
    if (len(l) < 11) :
        return [];
    connaissances = init_connaissances(l,r)
    a_ecarter = cherche_indesirable(connaissances)
    while a_ecarter is not None :
        if trace : print(f"{status(a_ecarter,connaissances)} => retiré de la liste");
        if (len(connaissances) <= 11) : #  comparaison ici au sens large car le test est fait avant la suppression
            return [];
        supprime_connaissance(a_ecarter,connaissances)
        a_ecarter = cherche_indesirable(connaissances)

    return list(connaissances.keys())


def repas_alice(l,r) :
    """
     fonction de lancement avec affichage du résultat
    """
    print(f"La liste de mes amis : {l}")
    invites = invites_possibles(l, r, trace=True)
    if len(invites) > 0 :
        print(f"La liste de mes {len(invites)} invités : {invites}")
    else :
        print("Je ne peux inviter personne")
        
        

def rel_2a2(l) :
    """
    construit une relation où chaque membre connaît chaque membre classé après lui
    NB : dans la clôture symétrique de cette relation, chaque membre connaît chaque autre membre
    """
    return [(x,y) for x in l for y in l if x<y]
              
    

# exemple

l1 = ['a','b','c','d', 'e','f']
l2 = ['g','h','i','j', 'k','l']
l3 = ['m','n','p']
l = l1+l2+l3

r = rel_2a2(l1) + rel_2a2(l2) + [('m',x) for x in l1+l2] + [('p',x) for x in 'a' 'b' 'i' 'j' 'm' ]

repas_alice(l,r)


